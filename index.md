# PCAP

Project to explore PCAP processing in C++, and also to use the latest C++ and tech stack.

## Building

I've used `ninja` as the underlying build system and also `mold` instead of the default linker. The build pipe uses latest `gcc` via the Docker image `deanturpin/gcc` -- see [Docker](https://hub.docker.com/r/deanturpin/gcc).

You can also build locally via the Docker image.

```bash
$ docker run -v .:/usr/src deanturpin/gcc make
cmake -B build -G Ninja -S .
-- The CXX compiler identification is GNU 14.0.1
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/local/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.5s)
-- Generating done (0.0s)
-- Build files have been written to: /usr/src/build
cmake --build build --parallel
[1/3] Generating std_modules_build_timestamp
[2/3] Building CXX object CMakeFiles/pcap.dir/main.cxx.o
[3/3] Linking CXX executable pcap
build/pcap
cya
```

Of course if you have the latest everything installed you don't need Docker. Below I have `gcc 14` installed from source on my laptop.

```bash
$ make
cmake -B build -G Ninja -S .
-- The CXX compiler identification is GNU 14.0.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/local/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.5s)
-- Generating done (0.0s)
-- Build files have been written to: /home/deanturpin/pcap/build
cmake --build build --parallel
[3/3] Linking CXX executable pcap
build/pcap
cya
```

## C++ modules

I got quite excited about C++ module support and spent a bit of time head-scratching before I noticed this: "Header units are not supported". Which is annoying because that was I was most interested in!  See the [CMake documentation](https://cmake.org/cmake/help/latest/manual/cmake-cxxmodules.7.html).

However, you can still build the headers seperately: I used [bravikov](https://github.com/bravikov/cmake-cpp-modules-example/tree/main) as a reference. But it is still early days for modules and you may find you have to revert to the old `#include` system for some libraries.

```bash
error: recursive lazy load
  648 |       iota_view(_Winc __value)
```
