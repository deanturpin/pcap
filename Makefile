all:
	cmake -B build -G Ninja -S .
	cmake --build build --parallel
	build/pcap

entr:
	ls *.cxx Makefile CMakeLists.txt | entr -cr make

clean:
	$(RM) -r build

