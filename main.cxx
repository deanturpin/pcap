// Using C++ modules
import <vector>;
import <cassert>;
import <cmath>;

// Too soon for some headers
#include <print>
#include <ranges>

// #embed </dev/urandom>

int main() {

  constexpr auto n{30uz};

  // Generate squares of an incrementing sequence
  // Reverse them and store in a container
  const auto xs =
      std::views::iota(1uz) | std::views::take(n) |
      std::views::transform([](const double i) { return std::pow(i, 2); }) |
      std::views::reverse | std::ranges::to<std::vector<double>>();

  assert(std::ranges::size(xs) == n);
  assert(*std::crbegin(xs) == 1uz);

  std::println("cya");
}
